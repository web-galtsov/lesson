import React  from "react";
import Typography from '@material-ui/core/Typography';
import {createMuiTheme, ThemeProvider} from "@material-ui/core/styles";

const theme = createMuiTheme({
    typography: {
        subtitle1: {
            fontSize: 12,
        },
        body1: {
            fontWeight: 500,
        },
        button: {
            fontStyle: 'italic',
        },
    },
});

export default function Fcomponent(props) {
    return (
        <ThemeProvider theme={theme}>
            <Typography align="center" variant="h2">Hello,{props.name}</Typography>
        </ThemeProvider>
    )
}
