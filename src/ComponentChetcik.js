import React, {Component} from "react";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {Typography,Box} from "@material-ui/core";


export default  class ComponentChetcik extends Component {
      constructor(props) {
        super (props);
        this.state = { count: 0   };
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.reset = this.reset.bind(this);

    };
      increment() {
        this.setState(state => ({count: state.count + 1 }));
    }
    decrement() {
        this.setState(state => ({ count: state.count - 1 }));
    }
    reset() {
        this.setState(state => ({ count: 0}));
    }


    render() {

        return (
            <>
            <Grid container
                  direction="row"
                  justify="center"
                  alignItems="center">
                <div>
                    <Button variant="outlined" onClick={this.increment} >Increment</Button>
                    <Button variant="outlined" color="primary" onClick={this.decrement}>Decrement</Button>
                    <Button variant="outlined" color="secondary" onClick={this.reset}>Reset</Button>
                </div>
            </Grid>
              <Box color="text.primary" >
                  <Typography align="center">
                <h4>Current: {this.state.count}</h4>
                    </Typography>
               </Box>
          </>
        )
    }
}
