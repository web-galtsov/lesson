import React, {Component} from "react";
import Typography from "@material-ui/core/Typography";
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

export default  class ComponentClass extends Component {
    constructor(props) {
        super (props);
 /*       this.state = {name: "Oxana"};*/
        this.state = {   visibility: false    };
        this.handleClick = this.handleClick.bind(this);
    };
 /*   handleClick(){
      this.setState({
          name: "Web Developer Blog"
      })
    }*/
    handleClick() {
        this.setState(state => ({ visibility: !state.visibility   }));
    }
    render() {
        if (this.state.visibility) {
            return (
                <Grid container spacing={1}>
                    <Grid item xs={12} sm={12}>
                        <Box bgcolor="primary.main" color="primary.contrastText" p={1}>
                           {/* <Typography variant="h5" align="center">Hello Component {this.state.name}</Typography>*/}
                            <Typography variant="h5" align="center">Now see me</Typography>
                        </Box>
                    </Grid>
                    <Grid container
                          direction="row"
                          justify="center"
                          alignItems="center">
                        <Box component="span" m={1}>
                            <Button variant="outlined" color="primary" onClick={this.handleClick}> Click</Button>
                        </Box>
                    </Grid>
                </Grid>
                /*   <Typography variant="h1" align="center">Hello Component {this.props.numbers.join(',')}</Typography>
                 <Typography variant="h1" align="center">Hello Component {this.props.name}</Typography>
                  <Typography variant="h1" align="center">Hello Component {name}</Typography>*/
            );
        } else  {
            return (
            <Grid container
                  direction="row"
                  justify="center"
                  alignItems="center">
                <Box component="span" m={1}>
                    <Button variant="outlined" color="primary" onClick={this.handleClick}> Click</Button>
                </Box>
            </Grid>
            )
        }
    }
}
/*
Component.defaultProps= {name: 'Oxana'}*/
