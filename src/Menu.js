import React from "react";
import Typography from '@material-ui/core/Typography';
import {createMuiTheme, ThemeProvider} from "@material-ui/core/styles";



const theme = createMuiTheme({
    typography: {
        subtitle1: {
            fontSize: 10,
        },
        body1: {
            fontWeight: 500,
        },
        button: {
            fontStyle: 'italic',
        },
    },
});
export const Menu = () => {
    return (
        <ThemeProvider theme={theme}>
            <Typography align="center" variant="h2">Hello Menu</Typography>
        </ThemeProvider>
    )
}
export default Menu